@echo off
:: put this file along with 'py_packages.txt' and 'wheels' in the directory where python is installed

:: assist file to install python packages
python.exe --version >NUL 2>&3

rem first update pip
python -m pip install --upgrade pip

rem FOR %%A IN (future futures) echo 
rem 'skip' # of rows
:: eol: '#' will be considered as comment
FOR /f "skip=2 eol=#" %%A IN (py_packages.txt) DO (
  scripts\pip install %%A
)


:: installing tensorflow using virtual environment
IF NOT EXIST venvtf\ (
echo creating new virtual environment for tensorflow
virtualenv --system-site-packages -p python ./venvtf
venvtf\scripts\activate
pip install --upgrade pip
pip install tensorflow
)


:: installing packages from wheels which are supposed to be present in subfolder wheels
for /f %%G in ('dir wheels\ /b') DO (
  scripts\pip install wheels\%%~G
)

:NOTPATH
::echo python exe not found

